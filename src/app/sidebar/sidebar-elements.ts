

export let sidebarElements =
    [
        {
        nombre: "Home",
        icono: "assets/sidebar-icons/home.png",
        route: "Home",
        posicion: "center"
    },
    {
        nombre: "About",
        icono: "assets/sidebar-icons/about.png",
        route: "About",
        posicion: "center"
    },
    {
        nombre: "Works",
        icono: "assets/sidebar-icons/works.png",
        route: "Works",
        posicion: "center"
    },
    {
        nombre: "Skills",
        icono: "assets/sidebar-icons/skills.png",
        route: "Skills",
        posicion: "center"
    },
    {
        nombre: "Contact",
        icono: "assets/sidebar-icons/contact.png",
        route: "Contact",
        posicion: "right"
    }
]